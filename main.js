const a = [1, 2, 3, "hello", 4, 5, 10];

const b = [1, 2, 3, true, 4, undefined, 6, 10];

function getSum(arr) {
  let sum = 0;
  for (let i = 0; i < arr.length; i++) {
    if (!isNaN(arr[i])) {
      if (arr[i] === true) continue;
      sum += arr[i];
    }
  }
  return sum;
}

const firstArr = getSum(a);
const secondArr = getSum(b);

function compare(sumA, sumB) {
  if (sumA < sumB) {
    return b;
  } else {
    return a;
  }
}

console.log(compare(firstArr, secondArr));
